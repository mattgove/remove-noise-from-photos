#!/usr/bin/env python3
"""
This script removes noise from an image by averaging multiple shots of
the same image. Before running this script, take at least 10 shots of the
same scene with your camera and save them to the folder denoted in the
"folder" variale. Do NOT just make copies of the image on your computer.

This is the best algorithm for removing noise from photos. 

Usage:
$ python3 remove-noise.py
"""
import os
import numpy as np
import cv2


# Define the path to the image where your original image is stored. You'll
# need to create the folder and load images into it if it doesn't exist.
folder = "noise-imgs"

# List all non-hidden files in the folder
files = [f for f in os.listdir(folder) if not f.startswith('.')]

# Define the path to the first image in the folder. When you're running
# this script, your single image should be the only image in the folder.
path = "{}/{}".format(folder, files[0])

# Initialize a variable that will store all of the data for the images
# you'll be averaging. This variable will also be used to calculate
# the average.
average = cv2.imread(path).astype(np.float)

# Read each image in the folder into OpenCV and add them to the "average"
# variable we defined in the previous step.
for file in files[1:]:
    path = "{}/{}".format(folder, file)
    image = cv2.imread(path)
    average += image

# Divide by the number of files in the directory to finish the calculation
# of the average value of all the photos. This removes the noise.
average /= len(files)

# Normalize the denoised output to ensure proper exposure and colors
output = cv2.normalize(average, None, 0, 255, cv2.NORM_MINMAX)

# Write the denoised output to a jpeg file.
cv2.imwrite("output.jpg", output)