# README #

This repository contains Python scripts that remove noise from photos using various averaging algorithms. 

### What is this repository for? ###

This repository is for individuals and organizations looking to boost the efficiency of their photography workflow with python. It is for educational purposes only.

### Required Packages and Libraries ###

The scripts in this repository require `numpy` and `cv2`, which is part of the OpenCV library.

### How do I get set up? ###

If you haven't yet installed the dependencies, please install them first. Open a Terminal or Command prompt and execute the following two commands.

```bash
pip3 install numpy
pip3 install opencv-python
```
To run the scripts, simply run them in a Terminal or Command Prompt.

```bash
python3 name-of-script.py
```


### Who do I talk to? ###

Please [contact Matt Gove](https://www.matthewgove.com/contact-me/) for all inquiries related to this software.

### Licensing ###
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see [https://www.gnu.org/licenses/](https://www.gnu.org/licenses/).