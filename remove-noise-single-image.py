#!/usr/bin/env python3
"""
This script removes noise from a single image using OpenCV's
cv2.fastNlMeansDenoisingColored() algorithm.

Use this if you only have a single image and cannot rephotograph
the image to create multiple copies of it.

Usage:
$ python3 remove-noise-single-image.py
"""
import os
import cv2


# Define the path to the image where your original image is stored. You'll
# need to create the folder and load images into it if it doesn't exist.
folder = "noise-imgs"

# List all non-hidden files in the folder
files = [f for f in os.listdir(folder) if not f.startswith('.')]

# Define the path to the first image in the folder. When you're running
# this script, your single image should be the only image in the folder.
path = "{}/{}".format(folder, files[0])

# Read the original image into the OpenCV library
original_img = cv2.imread(path)

# Denoise the Image
denoised_img = cv2.fastNlMeansDenoisingColored(original_img, None, 10, 10, 7, 15)

# Write the denoised image to a jpeg file
cv2.imwrite("output.jpg", denoised_img)